SHELL := /usr/bin/zsh
.PHONY: Berichtsheft Beispiel

Berichtsheft:
	ls Berichte/*/*.tex | awk '{printf "\\input{%s}\n", $$1}' > files.tex
	/usr/local/texlive/2017/bin/x86_64-linux/pdflatex Berichtsheft.tex

Beispiel:
	ls Berichte/Beispiel.tex | awk '{printf "\\input{%s}\n", $$1}' > files.tex
	/usr/local/texlive/2017/bin/x86_64-linux/pdflatex Berichtsheft.tex

Bericht:
	cp -n Berichte/Template.tex Berichte/$(shell date +"%Y")/$(shell date +"%V").tex
