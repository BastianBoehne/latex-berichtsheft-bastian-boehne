import json
import requests

config = {
    'token': 'ChaYYJiaC8KNPR8WQH7v7BEB',
    'user': 'bastian.boehne@real-digital.de',
    'domain': 'hitmeister.atlassian.net',
    'url': 'https://hitmeister.atlassian.net/rest/api/2/'
}


# format {'request': 'resource/query', 'params': {'expand': 'foo'}}
def request(data: dict = None) -> dict:
    requestUri = data.get('request')
    params = data.get('params', {})
    url = config['url'] + requestUri
    response = requests.get(url, auth=(config['user'], config['token']), params=params)
    responseParsed = json.loads(response.text)

    return responseParsed


def getIssue(number: int) -> dict:
    return request(
        {
            'request': 'issue/DEV-' + str(number),
            'params': {
                'expand': 'changelog',
            }
        }
    )


def getIssues(startAt: int = 0, maxResults: int = 100) -> dict:
    return request(
        {
            'request': 'search',
            'params': {
                'jql': 'assignee = currentuser()',
                'maxResults': maxResults,
                'startAt': startAt,
                'expand': 'changelog',
            }
        }
    )


def getAllIssues() -> list:
    total = None
    count = 0
    issues = []
    while total != count:
        response = getIssues(count)
        total = response.get('total')
        issues.extend(response.get('issues'))
        count = len(issues)

    return issues


