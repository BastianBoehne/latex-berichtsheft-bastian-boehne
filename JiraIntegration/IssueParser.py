from datetime import datetime


class Issue:

    ISO_8601 = '%Y-%m-%dT%H:%M:%S.%f%z'
    DATE_FORMAT = '%Y-%m-%d_%M'

    issueDict = None

    def __init__(self, issue: dict):
        self.issueDict = issue

    def extractData(self) -> dict:
        startDate = self.getDateStarted()
        if startDate is None:
            return {}
        startDate = self.dateFromString(startDate)
        dateFormatted = self.formatStringFromDate(startDate)

        return {
            dateFormatted: {
                'weekNr': startDate.strftime('%V'),
                'yearNr': startDate.strftime('%Y'),
                'weekDayNr': startDate.strftime('%w'),
                'date': startDate,
                'name': self.getName(),
                'headline': self.getHeadline(),
                'description': self.getDescription(),
            }
        }

    def extractResolvedData(self) -> dict:
        resolvedDate = self.getDateResolved()
        if resolvedDate is None:
            return {}
        resolvedDate = self.dateFromString(resolvedDate)
        dateFormatted = self.formatStringFromDate(resolvedDate)

        return {
            dateFormatted: {
                'date': resolvedDate,
                'name': self.getName(),
                'headline': self.getHeadline(),
            }
        }

    def getDateStarted(self) -> datetime:
        histories = self.issueDict.get('changelog', {}).get('histories')
        for hist in histories:
            items = hist.get('items', {})

            for item in items:
                if item.get('field') != 'status':
                    continue
                if item.get('fromString') == 'Open' and item.get('toString') == 'In Progress':
                    return hist.get('created')

    def getDateCreated(self) -> str:
        return self.issueDict.get('fields', {}).get('created')

    def getDateResolved(self) -> str:
        return self.issueDict.get('fields', {}).get('resolutiondate')

    def getName(self):
        return self.issueDict.get('key')

    def getHeadline(self):
        return self.issueDict.get('fields', {}).get('summary')

    def getDescription(self):
        return self.issueDict.get('fields', {}).get('description')

    def dateFromString(self, date: str) -> datetime:
        return datetime.strptime(date, self.ISO_8601)

    def formatStringFromDate(self, date: datetime) -> str:
        return date.strftime(self.DATE_FORMAT)
