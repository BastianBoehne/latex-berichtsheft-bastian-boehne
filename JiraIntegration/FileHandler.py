import os


rootDir = '/home/bab/src/latex-berichtsheft-bastian-boehne/BerichteGenerated/'

fileTemplate = r"""
\Titelzeile{%s}{%s}
\Woche{
\Tag{1}{8}{%s}
\Tag{2}{8}{%s}
\Tag{3}{8}{%s}
\Tag{4}{8}{%s}
\Tag{5}{8}{%s}
}
\Unterschrift
"""

textTemplate = r"""
%s (%s)
%s
"""


def createDir(name) -> str:
    if not os.path.exists(rootDir):
        os.makedirs(rootDir)
    dirToCreate = rootDir + name
    if not os.path.exists(dirToCreate):
        os.makedirs(dirToCreate)

    return dirToCreate + '/'


def createText(data):
    headline = data.get('headline', '')
    name = data.get('name', '')
    description = data.get('description', '')

    return textTemplate % (headline, name, description)


def writeTemplateFile(path: str, name: str, data: tuple):
    filledTemplate = (
        fileTemplate % (
            data[0],
            data[1],
            data[2],
            data[3],
            data[4],
            data[5],
            data[6],
        )
    )
    writeToFile(path, name, filledTemplate)


def writeToFile(path: str, name: str, content: str):
    file = open(path + name + '.tex', 'w+')
    file.write(content)
    file.close()


class Folder:

    files = {}

    path = None

    def __init__(self, year):
        self.path = createDir(year)


class File:

    file = {}

    path = None

    def addWeekdayText(self, text, weekDayNr):
        if self.file.get(weekDayNr, None) is not None:
            self.file[weekDayNr] += "\n\n"
            self.file[weekDayNr] += text
        else:
            self.file[weekDayNr] = text

    def write(self):
        if self.path is None:
            raise Exception('needs path to write to')

        writeTemplateFile(
            self.path,
            (
                self.file.get('1', ''),
                self.file.get('2', ''),
                self.file.get('3', ''),
                self.file.get('4', ''),
                self.file.get('5', ''),
            )
        )
