import API
from datetime import datetime
import FileHandler
from IssueParser import Issue


# directory = File.createDir('blub')
# File.writeToFile(directory, 'test', 'sdfsfd')

issueList = []
issueDictList = API.getAllIssues()

for issue in issueDictList:
    issueList.append(Issue(issue))

issueData = {}
issueResolvedData = {}

for issue in issueList:
    issueData.update(issue.extractData())
    issueResolvedData.update(issue.extractResolvedData())

folders = {}

for date, data in issueData.items():
    year = data.get('yearNr')
    week = data.get('weekNr')

    if folders.get(year) is None:
        folders[year] = FileHandler.Folder(year)

    if folders[year].files.get(week) is None:
        folders[year].files[week] = FileHandler.File()

    folders[year].files[week].addWeekdayText(FileHandler.createText(data), week)






bla = 'bla'

""" -- data needed
[
    weekNumber,
    yearNumber,
    text1,
    ...,
    text5,
]
"""
""" -- text contains

Issue1Headline (name)
Issue1Description

IssueResolvedHeadline (name)

"""
""" -- sort and assignment of issues
1. Only one issue per day
2. Issue 'work in progress' that day
3. Issue resolved that day
4. If two issues that day, push later 'wip' to next day

Dict mit {'YY-mm-dd:m': data}
"""
