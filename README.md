# Bastian Böhne

Dies ist mein Berichtsheft in Form eines Latex Projektes in einem Bitbucket Repository

# Vorschau

![Vorschau Berichtsheft](https://i.imgsafe.org/c5bfc2d41d.jpg)

# Generierung
Zum Einfügen aller vorhandenen Dateien in das Exportfile nutze den Unix Befehl `make`.


# Verwendung

Zur Erstellung einer Woche werden folgende Kommandos genutzt (Beispiel liegt unter Berichte/Beispiel.tex)

Die in spitzen Klammern stehenden Begriffe sind die Parameter.

    \Titelzeile{<KALENDERWOCHE>}{<JAHR>}{<AUSBILDUNGSNACHWEIS_NR>}
    \Woche{
    \Tag{<WOCHENTAGNUMMER(1-7)>}{<STUNDEN>}{<INHALT DER WOCHE>}
    }
    \Unterschrift

Außerdem sind zwei Metainformationen unter 
> Meta.tex

zu finden.

# Lizenz

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)  
Dieses LaTeX Berichtsheft von Bastian Böhne ist erstellt worden nach einer Vorlage von [Markus Amshove](http://amshove.org) und ist lizenziert unter der [Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz](http://creativecommons.org/licenses/by-sa/4.0/).
